import json
import requests
import ayar
from client import Client

import time
from datetime import datetime
from datetime import timedelta

class Kasif:
    def __init__(self,piyasa):
        self.piyasa = piyasa
        self.durum = "buyuk"
        self.client = None
        self.periyot = Client.KLINE_INTERVAL_1HOUR
        self.limit = 50
        self.tabloAdi = "Kasif"
        self.baglan()

    def baglan(self):
        try:
            self.client = Client(ayar.binanceKey, ayar.binanceSecret)
        except:
            print("Binance'e bağlanılamadı.")
            time.sleep(3)

    def tara(self):
        while(True):
            try:
                mumlar = self.client.get_klines(symbol=self.piyasa,interval=self.periyot,limit=self.limit)
                kisa = self.ortalama(mumlar,20)
                uzun = self.ortalama(mumlar,50)

                yeniDurum = "buyuk" if kisa > uzun else "kucuk"

                print("+" if yeniDurum=="buyuk" else "-",flush=True,end="")

                if self.durum == "kucuk" and yeniDurum=="buyuk":
                    #Al Sinyali
                    print("AL :",self.piyasa)
                    self.sinyal()
                    time.sleep(30*60)
                self.durum = yeniDurum

            except Exception as e:
                print("HATA :",e)
                time.sleep(3)
            time.sleep(5*60)
    
    def ortalama(self,dizi,adet):
        son = len(dizi)
        ilk = son - adet
        toplam = 0.0
        for i in range(ilk,son):
            toplam += float(dizi[i][4])
        return toplam/adet
    
    def sinyal(self):
        zaman = datetime.utcnow() + timedelta(hours=3)
        zamanText = zaman.strftime("%Y.%m.%d %H:%M:%S")
        fiyat = float(self.client.get_symbol_ticker(symbol=self.piyasa)["price"])
        data = {"zaman":zamanText,"piyasa":self.piyasa,"fiyat":fiyat}
        veri = json.dumps(data)

        link = ayar.appScriptBase+"?action=insert&table="+self.tabloAdi+"&data="+veri
        while(True):
            try:
                requests.get(link)
                break
            except Exception as e:
                print("HATA :",e)
                self.baglan()

            